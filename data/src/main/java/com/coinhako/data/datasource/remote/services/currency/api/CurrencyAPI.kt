package com.coinhako.data.datasource.remote.services.currency.api

import com.coinhako.data.models.response.GetCurrencyResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyAPI {
    @GET("price/all_prices_for_mobile")
    suspend fun getCurrency(@Query("counter_currency") counterCurency: String): GetCurrencyResponse
}