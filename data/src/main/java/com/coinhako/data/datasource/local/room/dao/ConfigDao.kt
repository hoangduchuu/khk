package com.coinhako.data.datasource.local.room.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.coinhako.data.models.dto.ConfigDTO
import kotlinx.coroutines.flow.Flow

@Dao
interface ConfigDao {

    @Query("SELECT * FROM config WHERE id = -1")
    fun get(): Flow<ConfigDTO>

    @Query("SELECT * FROM config WHERE id = -1")
    fun getSingle(): ConfigDTO

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(list: ConfigDTO)
}