package com.coinhako.data.datasource.remote.services.currency.api

import com.coinhako.data.utils.logger.ApiLogger
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class CurrencyServiceFactory {

    fun instance(): CurrencyAPI {
        val retrofit = Retrofit.Builder().apply {
            baseUrl("https://www.coinhako.com/api/v3/")
            client(setUpHttpClient())
            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            addConverterFactory(GsonConverterFactory.create(Gson()))
        }.build()
        return retrofit.create(CurrencyAPI::class.java)
    }

    private fun setUpHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor(ApiLogger())
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val customIntercep = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val newRequest = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                    .build()

                val response = chain.proceed(newRequest)
                return response
            }

        }
        return OkHttpClient.Builder().apply {
            interceptors().add(interceptor)
            interceptors().add(customIntercep)
            connectTimeout(5, TimeUnit.SECONDS)
            readTimeout(5, TimeUnit.SECONDS)

        }.build()
    }

}