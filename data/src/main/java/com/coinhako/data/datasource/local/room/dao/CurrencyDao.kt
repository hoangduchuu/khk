package com.coinhako.data.datasource.local.room.dao

import androidx.annotation.WorkerThread
import androidx.room.*
import com.coinhako.data.models.dto.CurrencyDTO
import kotlinx.coroutines.flow.Flow

@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<CurrencyDTO>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: CurrencyDTO?)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(list: List<CurrencyDTO>?)

    @Query("SELECT * FROM currency WHERE base=:baseName LIMIT 1")
    suspend fun getByBaseId(baseName: String): CurrencyDTO

    @WorkerThread
    @Query("SELECT * FROM currency")
    fun loadAll(): Flow<List<CurrencyDTO>>

    @WorkerThread
    @Query("SELECT * FROM currency")
    fun getSingle(): List<CurrencyDTO>

    @Query("UPDATE currency set is_bookmarked = :status where base = :currencyName ")
    suspend fun bookMark(status: Boolean, currencyName: String)

    @Query("UPDATE currency set is_bookmarked = :status where base = :currencyName")
    suspend fun unBookMark(status: Boolean, currencyName: String)
}

