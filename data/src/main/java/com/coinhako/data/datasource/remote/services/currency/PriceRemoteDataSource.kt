package com.coinhako.data.datasource.remote.services.currency

import com.coinhako.data.models.dto.CurrencyDTO

interface PriceRemoteDataSource {
    suspend fun getCurrency(counterCurrency: String): List<CurrencyDTO>
}