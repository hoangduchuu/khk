package com.coinhako.data.datasource.local.price

import com.coinhako.data.models.dto.CurrencyDTO
import kotlinx.coroutines.flow.Flow

interface PriceLocalDataSource {
     fun getCurrency(): Flow<List<CurrencyDTO>>

    suspend fun insert(list: List<CurrencyDTO>?)

    suspend fun bookmark(currencyName: String): Boolean
    suspend fun unBookmark(currencyName: String): Boolean
}