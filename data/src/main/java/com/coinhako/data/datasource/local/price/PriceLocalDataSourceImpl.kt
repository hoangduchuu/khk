package com.coinhako.data.datasource.local.price

import com.coinhako.data.datasource.local.room.app.AppDatabase
import com.coinhako.data.models.dto.CurrencyDTO
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow

class PriceLocalDataSourceImpl(private val appDatabase: AppDatabase) : PriceLocalDataSource {
    override fun getCurrency(): Flow<List<CurrencyDTO>> {
        return appDatabase.currencyDao.loadAll()
    }

    /**
     * Insert or update to Sqlite database
     */
    @DelicateCoroutinesApi
    override suspend fun insert(list: List<CurrencyDTO>?) {
        val db = appDatabase.currencyDao.getSingle()

        if (db.isEmpty()) {
            appDatabase.currencyDao.insertAll(list)
        } else {
            val temp = mutableListOf<CurrencyDTO>()
            list?.forEach {
                val existItem = appDatabase.currencyDao.getByBaseId(it.base)
                temp.add(it.copy(isBookmarked = existItem.isBookmarked))

            }
            appDatabase.currencyDao.insertAll(temp)

        }
    }

    override suspend fun bookmark(currencyName: String): Boolean {
        appDatabase.currencyDao.bookMark(true, currencyName)
        return true
    }

    override suspend fun unBookmark(currencyName: String): Boolean {
        appDatabase.currencyDao.unBookMark(false, currencyName)
        return true
    }

}