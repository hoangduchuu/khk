package com.coinhako.data.datasource.local.config

import com.coinhako.data.models.dto.ConfigDTO
import kotlinx.coroutines.flow.Flow

interface ConfigDataSource {
    suspend fun isCacheExpired(): Boolean
    suspend fun save(configDTO: ConfigDTO)
    suspend fun get(): Flow<ConfigDTO>
}