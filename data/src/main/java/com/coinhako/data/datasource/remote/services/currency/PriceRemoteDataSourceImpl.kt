package com.coinhako.data.datasource.remote.services.currency

import com.coinhako.data.datasource.remote.services.currency.api.CurrencyAPI
import com.coinhako.data.models.dto.CurrencyDTO
import com.example.domain.exception.BusinessException
import java.lang.Exception

class PriceRemoteDataSourceImpl(private val service: CurrencyAPI) : PriceRemoteDataSource {
    override suspend fun getCurrency(counterCurrency: String): List<CurrencyDTO> {
        return service.getCurrency(counterCurrency).data
    }
}