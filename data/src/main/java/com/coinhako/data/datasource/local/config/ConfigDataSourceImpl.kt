package com.coinhako.data.datasource.local.config

import com.coinhako.data.datasource.local.room.app.AppDatabase
import com.coinhako.data.models.dto.ConfigDTO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ConfigDataSourceImpl(private val appDatabase: AppDatabase) : ConfigDataSource {
    override suspend fun isCacheExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val expirationTime = (30_000).toLong()
        return currentTime - getSingle().lastCacheTime > expirationTime
    }

    override suspend fun save(configDTO: ConfigDTO) {
        return appDatabase.configDao.insert(configDTO)
    }

    override suspend fun get(): Flow<ConfigDTO> {
        return appDatabase.configDao.get()
    }

    private suspend fun getSingle(): ConfigDTO {
        return appDatabase.configDao.getSingle()
    }
}