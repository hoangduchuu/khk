package com.coinhako.data.datasource.local.room.app

import androidx.room.Database
import androidx.room.RoomDatabase
import com.coinhako.data.models.dto.CurrencyDTO
import com.coinhako.data.datasource.local.room.app.AppDatabase.Companion.DB_VERSION
import com.coinhako.data.datasource.local.room.dao.ConfigDao
import com.coinhako.data.datasource.local.room.dao.CurrencyDao
import com.coinhako.data.models.dto.ConfigDTO

@Database(
    entities = [CurrencyDTO::class, ConfigDTO::class],
    version = DB_VERSION,
    exportSchema = false,
)
abstract class AppDatabase : RoomDatabase() {

    abstract val currencyDao: CurrencyDao
    abstract val configDao: ConfigDao

    companion object {
        const val DB_NAME = "CoinHako.db"
        const val DB_VERSION = 2
    }
}