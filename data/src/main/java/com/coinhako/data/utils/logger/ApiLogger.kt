package com.coinhako.data.utils.logger

import android.util.Log
import com.vivacos.utility.logger.CustomLogger
import okhttp3.logging.HttpLoggingInterceptor


//https://stackoverflow.com/questions/35006664/how-to-pretty-print-in-retrofit-2
class ApiLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        if (message.startsWith("{") || message.startsWith("[")) {
            try {
                CustomLogger.json(message)
            } catch (m: Exception) {
                Log.w("API exception:", m.toString())
            }
        } else {
            Log.w("API:", message)
            return
        }
    }
}