package com.coinhako.data.repository

import com.coinhako.data.datasource.local.price.PriceLocalDataSource
import com.coinhako.data.datasource.remote.services.currency.PriceRemoteDataSource
import com.example.domain.entity.CurrencyEntity
import com.example.domain.repo.PriceRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.lang.Exception

class PriceRepositoryImpl(
    private val local: PriceLocalDataSource,
    private val remote: PriceRemoteDataSource
) : PriceRepository {
    override fun getCurrency(): Flow<List<CurrencyEntity>> {
        return local.getCurrency().map { it.map { item -> item.toEntity() } }

    }

    override suspend fun fetchRemote(counterCurrency: String): List<CurrencyEntity> {
        remote.getCurrency("USD").also {
            local.insert(it)
            return it.map { photoResponse ->
                photoResponse.toEntity()
            }
        }
    }

    override suspend fun bookmarkCurrency(newStatus: Boolean, currencyName: String): Boolean {
        if (newStatus) return local.bookmark(currencyName)
        return local.unBookmark(currencyName)
    }

}