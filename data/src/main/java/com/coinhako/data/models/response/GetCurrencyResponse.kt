package com.coinhako.data.models.response

import com.coinhako.data.models.dto.CurrencyDTO

data class GetCurrencyResponse(val data: List<CurrencyDTO>)
