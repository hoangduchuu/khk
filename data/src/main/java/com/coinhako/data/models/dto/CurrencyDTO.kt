package com.coinhako.data.models.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.domain.entity.CurrencyEntity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "currency")
data class CurrencyDTO(
    @PrimaryKey
    val base: String = "",
    val counter: String? = null,
    @SerializedName("buy_price")

    @ColumnInfo(name = "buy_price")
    val buyPrice: String? = null,
    @SerializedName("sell_price")
    @ColumnInfo(name = "sell_price")
    val sellPrice: String? = null,
    val icon: String? = null,
    val name: String? = null,
    @ColumnInfo(name = "is_bookmarked")
    var isBookmarked: Boolean
) {
    fun toEntity(): CurrencyEntity {
        return CurrencyEntity(base, counter, buyPrice, sellPrice, icon, name, isBookmarked)
    }
}