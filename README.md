# Pre-test - Currency App  
  
Currency App is an android app that part of a wallet to view the price of crypto currencies.  
  
## User Stories  
  
The following **Required** functionality is completed:  
* [x] Implement MVVM Architecture(For UI layer) and Clean architecture for App's Architecture.  
* [x] Implement coroutines & Koin (dependency injection).  
* [x] Add update price real time (each 30s)  
* [x] Add search functions for names to find currencies.  
  
The following **Bonus / optional task** features are implemented:  
* [x] Provide Unit Testing.  
* [ ] Provide UI Testing.  
* [x] Try to go the extra mile and create a real WOW effect UX.  
  
The following **Additional** features are implemented: 
* [x] User can bookmark the currency
* [ ] View the data as sorted by base name, sell price, buy price           
  
## Video Walkthrough .  
  
Here's a walkthrough of implemented user stories:  
  
[![](https://i.imgur.com/mwrQEWS.png)](https://gitlab.com/hoangduchuu/khk/-/blob/develop/assets/walkthrough.mp4)  
  
## Notes  
  
Take time to get knowledge about Coroutines but still don't understand it really, Because I used RxJava to implement the Background task before. 
This is the first time using the Coroutines  
## License  
  
 Copyright [yyyy] [name of copyright owner]  
 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at  
 http://www.apache.org/licenses/LICENSE-2.0  
 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
