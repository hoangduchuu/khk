package com.example.coinhako.app

import android.app.Application
import com.example.coinhako.di.AppModule
import com.example.coinhako.di.DataModule
import com.example.coinhako.di.DomainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class CoiHakoApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        setUpDI();
    }

    private fun setUpDI() {
        startKoin {
            androidLogger(level = Level.DEBUG)
            androidContext(this@CoiHakoApplication)
            modules(listOf(AppModule, DataModule, DomainModule))
        }
    }
}