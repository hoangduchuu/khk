package com.example.coinhako.base

enum class RetryDialogState { IDLE, SHOW, CLOSE }