package com.example.coinhako.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    private var shouldShowLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    private var shouldShowRetryRetryState: MutableLiveData<RetryDialogState> =
        MutableLiveData(RetryDialogState.IDLE)

    fun isLoading(): MutableLiveData<Boolean> {
        return shouldShowLoading
    }

    fun isShowRetryDialog(): MutableLiveData<RetryDialogState> {
        return shouldShowRetryRetryState
    }

    protected fun showLoading() {
//        shouldShowLoading.postValue(true)
        shouldShowLoading.value = true
    }

    protected fun hideLoading() {
//        shouldShowLoading.postValue(false)
        shouldShowLoading.value = false
    }

    protected fun showRetryDialog() {
        shouldShowRetryRetryState.value = RetryDialogState.SHOW
    }

    fun hideRetryDialog() {
        shouldShowRetryRetryState.value = RetryDialogState.CLOSE
    }
}