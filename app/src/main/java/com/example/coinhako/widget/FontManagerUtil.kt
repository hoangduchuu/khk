package com.example.coinhako.widget

import android.content.Context
import android.graphics.Typeface

class FontManagerUtil {
    companion object {
        val REGULAR_FONT_PATH = "fonts/Roboto-Regular.ttf"
        val BOLD_FONT_PATH = "fonts/Roboto-Bold.ttf"
        val SEMIBOLD_FONT_PATH = "fonts/Roboto-Italic.ttf"
        var semiBoldFont: Typeface? = null
        var boldFont: Typeface? = null
        var regularFont: Typeface? = null

        fun getFontRegular(context: Context): Typeface? {
            if (regularFont == null) {
                regularFont = Typeface.createFromAsset(context.assets, REGULAR_FONT_PATH)
            }
            return regularFont
        }

        fun getFontSemiBold(context: Context): Typeface? {
            if (semiBoldFont == null) {
                semiBoldFont = Typeface.createFromAsset(context.assets, SEMIBOLD_FONT_PATH)
            }
            return semiBoldFont
        }

        fun getFontBold(context: Context): Typeface? {
            if (boldFont == null) {
                boldFont = Typeface.createFromAsset(context.assets, BOLD_FONT_PATH)
            }
            return boldFont
        }
    }
}
