package com.example.coinhako.widget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.example.coinhako.R

open class CustomFontTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        setupView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        setupView(context, attrs)
    }

    /**
     * Process properties (from android layout) and provide default property values
     */
    private fun setupView(context: Context, attrs: AttributeSet?) {
        val typedArray: TypedArray =
            context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView)

        val fontId = typedArray.getInt(R.styleable.CustomFontTextView_fontName, 3)
        when (fontId) {
            1 -> this.setTypeface(FontManagerUtil.boldFont, Typeface.BOLD)
            2 -> this.setTypeface(FontManagerUtil.semiBoldFont, Typeface.BOLD)
            3 -> this.typeface = FontManagerUtil.regularFont
        }
        typedArray.recycle()
    }
}
