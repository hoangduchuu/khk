package com.example.coinhako.di

import androidx.room.Room
import com.coinhako.data.datasource.local.config.ConfigDataSource
import com.coinhako.data.datasource.local.config.ConfigDataSourceImpl
import com.coinhako.data.datasource.local.price.PriceLocalDataSource
import com.coinhako.data.datasource.local.price.PriceLocalDataSourceImpl
import com.coinhako.data.repository.PriceRepositoryImpl
import com.coinhako.data.datasource.local.room.app.AppDatabase
import com.coinhako.data.datasource.remote.services.currency.PriceRemoteDataSource
import com.coinhako.data.datasource.remote.services.currency.PriceRemoteDataSourceImpl
import com.coinhako.data.datasource.remote.services.currency.api.CurrencyAPI
import com.coinhako.data.datasource.remote.services.currency.api.CurrencyServiceFactory
import com.example.coinhako.currency.CounterCurencyVM
import com.example.domain.repo.PriceRepository
import com.example.domain.usecase.runner.BookMarkCurrencyUseCase
import com.example.domain.usecase.runner.FetchCurrencyUseCase
import com.example.domain.usecase.runner.GetCurrencyUseCase
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val DataModule = module {
    // local data
    single { Room.databaseBuilder(get(), AppDatabase::class.java, AppDatabase.DB_NAME).allowMainThreadQueries().build() }

    // network service
    single<CurrencyAPI> { CurrencyServiceFactory().instance() }

    //remote
    single<PriceRemoteDataSource> { PriceRemoteDataSourceImpl(get()) }

    //local
    single<PriceLocalDataSource> { PriceLocalDataSourceImpl(get()) }
    single<ConfigDataSource> { ConfigDataSourceImpl(get()) }

    // repository
    single<PriceRepository> { PriceRepositoryImpl(get(), get()) }
}

val AppModule = module {
    //vm
    viewModel { CounterCurencyVM(get(),get(),get()) }
}

val DomainModule = module {
    //usecase
    single { GetCurrencyUseCase(get()) }
    single { FetchCurrencyUseCase(get()) }
    single { BookMarkCurrencyUseCase(get()) }
}