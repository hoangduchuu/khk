package com.example.coinhako.currency

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncDifferConfig
import com.example.domain.entity.CurrencyEntity
import java.util.concurrent.Executors
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.coinhako.R
import java.util.*
import kotlin.collections.ArrayList


class CurrencyAdapter constructor(
    context: Context,
    var listener: CurrencyListener? = null

) : ListAdapter<CurrencyEntity, CurrencyAdapter.ViewHolder>(
    AsyncDifferConfig.Builder(CurrencyDiffCallback())
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
), Filterable {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    var list = mutableListOf<CurrencyEntity>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflater.inflate(R.layout.item, parent, false)
        return ViewHolder(itemView)
    }


    fun setData(currency: MutableList<CurrencyEntity>?) {
        if (currency != null) {
            this.list = currency
            submitList(currency)
        }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvName: TextView = itemView.findViewById(R.id.tvName)
        private val tvBaseName: TextView = itemView.findViewById(R.id.tvBaseName)
        private val tvBuyPrice: TextView = itemView.findViewById(R.id.tvBuyPrice)
        private val tvSalePrice: TextView = itemView.findViewById(R.id.tvSalePrice)


        @SuppressLint("SetTextI18n")
        fun onBind(currency: CurrencyEntity) {
            tvName.text = currency.name
            tvBaseName.text = currency.base
            tvBuyPrice.text = "${currency.buyPrice}"
            tvSalePrice.text = "${currency.sellPrice}"

            Glide.with(itemView)
                .load(currency.icon)
                .into(itemView.findViewById(R.id.ivCoinLogo))
            val bookmarkItem = itemView.findViewById<ImageView>(R.id.bookMark)

            if (currency.bookMarked == true) {
                bookmarkItem.background =
                    ContextCompat.getDrawable(itemView.context, android.R.drawable.star_on)
            } else {
                bookmarkItem.background =
                    ContextCompat.getDrawable(itemView.context, android.R.drawable.star_off)
            }

            bookmarkItem.setOnClickListener {
                listener?.onBookmarkClicked(currency)
            }
            bookmarkItem.setOnLongClickListener {
                listener?.onBookMarkLongClicked(currency)
                return@setOnLongClickListener true
            }
        }
    }

    override fun submitList(list: List<CurrencyEntity>?) {
        super.submitList(ArrayList<CurrencyEntity>(list ?: listOf()))
    }

    override fun getFilter(): Filter {
        return customFilter
    }

    private val customFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredList = mutableListOf<CurrencyEntity>()
            if (constraint == null || constraint.isEmpty()) {
                filteredList.addAll(list)
            } else {
                if (constraint.isNotEmpty() && constraint == "liked") {
                    for (item in list) {
                        val condition1 = item.bookMarked == true
                        if (condition1) filteredList.add(item)
                    }
                } else {
                    for (item in list) {
                        val condition1 = item.base?.toLowerCase(Locale.ROOT)
                            ?.contains(constraint.toString().toLowerCase(Locale.ROOT)) == true
                        val condition2 = item.name?.toLowerCase(Locale.ROOT)
                            ?.contains(constraint.toString().toLowerCase(Locale.ROOT)) == true
                        if (condition1 || condition2) filteredList.add(item)
                    }
                }


            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence?, filterResults: FilterResults?) {
            submitList(filterResults?.values as List<CurrencyEntity>?)
        }

    }

}