package com.example.coinhako.currency

import com.example.domain.entity.CurrencyEntity

interface CurrencyListener {
    fun onBookmarkClicked(currency: CurrencyEntity)
    fun onBookMarkLongClicked(currency: CurrencyEntity)
}