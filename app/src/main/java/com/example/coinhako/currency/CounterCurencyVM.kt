package com.example.coinhako.currency

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.coinhako.base.BaseViewModel
import com.example.domain.entity.CurrencyEntity
import com.example.domain.usecase.base.BaseUseCase
import com.example.domain.usecase.callback.UseCaseCallBack
import com.example.domain.usecase.runner.BookMarkCurrencyUseCase
import com.example.domain.usecase.runner.FetchCurrencyUseCase
import com.example.domain.usecase.runner.GetCurrencyUseCase
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CounterCurencyVM constructor(
    private val getPostsUseCase: GetCurrencyUseCase,
    private val fetchCurrencyUseCase: FetchCurrencyUseCase,
    private val bookMarkCurrencyUseCase: BookMarkCurrencyUseCase,
) :
    BaseViewModel() {

    var currencies: MutableLiveData<List<CurrencyEntity>> = MutableLiveData()

    @ObsoleteCoroutinesApi
    private val tickerChannel = ticker(delayMillis = 30_000, initialDelayMillis = 0)

    @RequiresApi(Build.VERSION_CODES.O)
    fun getCurrency() {
        executeFetchCurrencyFromAPI(withProgress = true)
    }

    @ObsoleteCoroutinesApi
    fun autoFetchCurrency() {
        viewModelScope.launch {
            for (event in tickerChannel) executeFetchCurrencyFromAPI()
        }

    }

    fun bookMark(nextStatus: Boolean, currencyName: String) {
        bookMarkCurrencyUseCase.autoRun(
            viewModelScope,
            BookMarkCurrencyUseCase.Params(nextStatus, currencyName)
        )
    }

    private fun executeGetCurrency() {
        getPostsUseCase(
            viewModelScope,
            BaseUseCase.NoParams(),
            object : UseCaseCallBack<Flow<List<CurrencyEntity>>> {
                override suspend fun onSuccess(result: Flow<List<CurrencyEntity>>) {
                    result.collect { currencies.value = it }
                }

                override fun onError(exception: Exception?) {}

                override fun onComplete() {}

            })
    }

    private fun executeFetchCurrencyFromAPI(withProgress: Boolean? = false) {
        if (withProgress == true) {
            showLoading()
        }
        fetchCurrencyUseCase(
            viewModelScope,
            FetchCurrencyUseCase.Params("USD"),
            object : UseCaseCallBack<List<CurrencyEntity>> {
                override suspend fun onSuccess(result: List<CurrencyEntity>) {
                    executeGetCurrency()
                    hideLoading()
                }

                override fun onError(exception: Exception?) {
                    hideLoading()
                    if (withProgress == true) {
                        showRetryDialog()
                    }
                }

                override fun onComplete() {
                    hideLoading()
                }

            })
    }


}