package com.example.coinhako.currency

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.coinhako.R
import com.example.coinhako.base.RetryDialogState
import com.example.coinhako.utils.hideKeyboard
import com.example.coinhako.widget.CurrencyDividerItemDecoration
import com.example.domain.entity.CurrencyEntity
import com.example.flatdialoglibrary.dialog.FlatDialog
import com.shashank.sony.fancydialoglib.Animation
import com.shashank.sony.fancydialoglib.FancyAlertDialog
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener
import kotlinx.coroutines.ObsoleteCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel


@ObsoleteCoroutinesApi
class CurrencyActivity : AppCompatActivity() {
    private val vm: CounterCurencyVM by viewModel()
    private val adapter by lazy { CurrencyAdapter(this, currencyItemCLickListener) }
    private lateinit var edtSearch: EditText
    val flatDialog by lazy { FlatDialog(this) }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)

        vm.getCurrency()
        vm.autoFetchCurrency()
        initViews()
        observerLiveData()
    }

    private fun observerLiveData() {
        vm.currencies.observe(this, {
            adapter.setData(it as MutableList<CurrencyEntity>?)
            adapter.filter.filter(edtSearch.text)
        })

        vm.isLoading().observe(this, {
            if (it == true) findViewById<ProgressBar>(R.id.pbLoading).visibility =
                View.VISIBLE else findViewById<ProgressBar>(R.id.pbLoading).visibility = View.GONE
        })

        vm.isShowRetryDialog().observe(this, {
            if (it == RetryDialogState.SHOW) {
                flatDialog.setTitle(getString(R.string.error_message))
                    .setFirstButtonText(getString(R.string.retry))
                    .setSecondButtonText(getString(R.string.exit))
                    .withFirstButtonListner {
                        vm.getCurrency()
                        vm.autoFetchCurrency()
                        vm.hideRetryDialog()
                    }
                    .withSecondButtonListner {
                        finish()
                    }
                    .show()

            }
            if (it == RetryDialogState.CLOSE || it == RetryDialogState.IDLE) {
                flatDialog.dismiss()
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {
        setupListView()
        setupSearchButton()

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupSearchButton() {
        edtSearch = findViewById(R.id.edtSearh)
        edtSearch.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    hideKeyboard()
                    true
                }
                else -> false
            }
        }

        edtSearch.doOnTextChanged { text, _, _, count ->
            adapter.filter.filter(text)
            val clearButton = android.R.drawable.ic_menu_close_clear_cancel
            val leftIcon = android.R.drawable.ic_menu_search
            if (count > 0) {
                edtSearch.setCompoundDrawablesWithIntrinsicBounds(leftIcon, 0, clearButton, 0)

            } else {
                edtSearch.setCompoundDrawablesWithIntrinsicBounds(leftIcon, 0, 0, 0)
            }
        }
        edtSearch.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, event: MotionEvent?): Boolean {
                try {
                    if (event?.action == MotionEvent.ACTION_UP) {
                        if (event.rawX >= (edtSearch.getRight() - edtSearch.compoundDrawables[2].bounds.width())) {
                            val leftIcon = android.R.drawable.ic_menu_search
                            edtSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                            edtSearch.setText("")
                            return true;
                        }
                    }
                } catch (ex: Exception) {

                }
                return false
            }


        })
        edtSearch.background.clearColorFilter();
    }

    private fun setupListView() {
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)


        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(
            baseContext,
            R.drawable.line_divider
        )?.let {
            divider.setDrawable(
                it
            )
        };

        recyclerView.addItemDecoration(
            CurrencyDividerItemDecoration(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.line_divider,
                )
            )
        )
    }

    private val currencyItemCLickListener by lazy {
        object : CurrencyListener {
            override fun onBookmarkClicked(currency: CurrencyEntity) {
                currency.bookMarked?.let { it1 ->
                    currency.base?.let { it2 ->
                        vm.bookMark(
                            nextStatus = !it1,
                            currencyName = it2
                        )
                    }
                }
            }

            override fun onBookMarkLongClicked(currency: CurrencyEntity) {
                Toast.makeText(baseContext, "Bookmark", Toast.LENGTH_SHORT).show()
            }

        }
    }

}