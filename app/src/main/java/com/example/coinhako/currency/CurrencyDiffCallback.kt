package com.example.coinhako.currency

import androidx.recyclerview.widget.DiffUtil
import com.example.domain.entity.CurrencyEntity

class CurrencyDiffCallback : DiffUtil.ItemCallback<CurrencyEntity>() {
    override fun areItemsTheSame(oldItem: CurrencyEntity, newItem: CurrencyEntity): Boolean =
        oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: CurrencyEntity, newItem: CurrencyEntity): Boolean =
        oldItem == newItem

}