package com.example.coinhako

import com.example.domain.entity.CurrencyEntity
import com.example.domain.exception.BusinessException
import com.example.domain.repo.PriceRepository
import com.example.domain.usecase.base.BaseUseCase
import com.example.domain.usecase.runner.FetchCurrencyUseCase
import com.example.domain.usecase.runner.GetCurrencyUseCase
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.lang.Exception

class CurrencyVmUnitest {


    @MockK
    lateinit var priceRepositoryImpl: PriceRepository

    lateinit var fetchUseCase: FetchCurrencyUseCase

    lateinit var getCurrencyUseCase: GetCurrencyUseCase


    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        fetchUseCase = FetchCurrencyUseCase(priceRepositoryImpl)

        getCurrencyUseCase = GetCurrencyUseCase(priceRepositoryImpl)

    }

    @Test()
    fun `fetch remote data from use case then return the data from repo`() = runBlocking {
//        val currency = CurrencyTestMock.list()
        val currency = CurrencyTestMock.list()
        every { runBlocking { priceRepositoryImpl.fetchRemote("USD") } } returns (currency)
        val output = fetchUseCase.run(FetchCurrencyUseCase.Params("USD"))
        print(output)
        MatcherAssert.assertThat(
            currency,
            CoreMatchers.`is`(output)
        )
    }

    @Test()
    fun `observe local data from use case then return the data from repo`() = runBlocking {
        val currency = mockk<Flow<List<CurrencyEntity>>>()
        every { runBlocking { priceRepositoryImpl.getCurrency() } } returns (currency)
        val output = getCurrencyUseCase.run(BaseUseCase.NoParams())
        print(output)
        MatcherAssert.assertThat(
            currency,
            CoreMatchers.`is`(output)
        )
    }
}