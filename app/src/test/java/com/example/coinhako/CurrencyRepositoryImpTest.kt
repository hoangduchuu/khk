package com.example.coinhako

import com.example.domain.entity.CurrencyEntity
import com.example.domain.repo.PriceRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test


class CurrencyRepositoryImpTest {

    @MockK
    lateinit var priceRepositoryImpl: PriceRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this) //for initialization
    }


    @Test()
    fun `fetchAPI return the match data `() = runBlocking {
        val currency = CurrencyTestMock.list()
        every { runBlocking { priceRepositoryImpl.fetchRemote("") } } returns (currency)

        val result = priceRepositoryImpl.fetchRemote("")
        MatcherAssert.assertThat(result, CoreMatchers.`is`(currency))
    }

    @Test()
    fun `get Data return mach data`() = runBlocking {
        val currency = mockk<Flow<List<CurrencyEntity>>>()
        every { runBlocking { priceRepositoryImpl.getCurrency() } } returns (currency)

        val result = priceRepositoryImpl.getCurrency()
        MatcherAssert.assertThat(result, CoreMatchers.`is`(currency))
    }
}