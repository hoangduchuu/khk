package com.example.coinhako

import com.example.domain.entity.CurrencyEntity

object CurrencyTestMock {
    fun list(): List<CurrencyEntity> {
        return mutableListOf(
            CurrencyEntity(
                base = "1",
                counter = "1",
                buyPrice = "1",
                sellPrice = "1",
                icon = "1",
                name = "1",
                bookMarked = true
            ),
            CurrencyEntity(
                base = "2",
                counter = "3",
                buyPrice = "3",
                sellPrice = "14",
                icon = "12",
                name = "4",
                bookMarked = false
            )
        )
    }
}