package com.example.domain.entity

/**
 * Koi
 */
data class CurrencyEntity(
    val base: String? = null,
    val counter: String? = null,
    val buyPrice: String? = null,
    val sellPrice: String? = null,
    val icon: String? = null,
    val name: String? = null,
    val bookMarked:Boolean?=false
)