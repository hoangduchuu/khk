package com.example.domain.exception

class BusinessException(override val message: String? = null, val code: Int? = 0) : Exception() {
    companion object {
        private const val BASE_EXCEPTION_CODE = 0;
        const val UN_KNOW = BASE_EXCEPTION_CODE + 1;
        const val TIMEOUT = BASE_EXCEPTION_CODE + 2;
        const val IO = BASE_EXCEPTION_CODE + 3;
        const val GET_CURRENCY_ERROR = BASE_EXCEPTION_CODE + 4;
    }

}