package com.example.domain.func

import com.example.domain.exception.BusinessException
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * Mapping exception
 */
fun handleException(throwable: Throwable?): BusinessException {
    return when (throwable) {

        is SocketTimeoutException -> {
            BusinessException(throwable.message, BusinessException.TIMEOUT)
        }

        is IOException -> {
            BusinessException(throwable.message, BusinessException.IO)
        }

        else -> BusinessException(throwable?.message, BusinessException.UN_KNOW);
    }
}