package com.example.domain.repo

import com.example.domain.entity.CurrencyEntity
import kotlinx.coroutines.flow.Flow

interface PriceRepository {
    fun getCurrency(): Flow<List<CurrencyEntity>>

    suspend fun fetchRemote(counterCurrency: String): List<CurrencyEntity>

    suspend fun bookmarkCurrency(newStatus: Boolean, currencyName: String): Boolean
}