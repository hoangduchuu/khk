package com.example.domain.usecase.runner

import com.example.domain.entity.CurrencyEntity
import com.example.domain.repo.PriceRepository
import com.example.domain.usecase.base.BaseUseCase

class FetchCurrencyUseCase constructor(private val postsRepository: PriceRepository) :
    BaseUseCase<List<CurrencyEntity>, FetchCurrencyUseCase.Params>() {

    override suspend fun run(params: Params): List<CurrencyEntity> {
        return postsRepository.fetchRemote(params.counterCurrency)
    }

    class Params(val counterCurrency: String)

}