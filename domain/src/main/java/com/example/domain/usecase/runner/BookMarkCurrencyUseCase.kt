package com.example.domain.usecase.runner

import com.example.domain.repo.PriceRepository
import com.example.domain.usecase.base.BaseUseCase

class BookMarkCurrencyUseCase constructor(private val postsRepository: PriceRepository) :
    BaseUseCase<Boolean, BookMarkCurrencyUseCase.Params>() {

    override suspend fun run(params: Params): Boolean {
        return postsRepository.bookmarkCurrency(params.nextStatus, params.currencyName)
    }

    class Params(val nextStatus: Boolean, val currencyName: String)

}