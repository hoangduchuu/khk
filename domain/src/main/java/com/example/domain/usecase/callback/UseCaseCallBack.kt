package com.example.domain.usecase.callback


interface UseCaseCallBack<T> {
    suspend fun onSuccess(result: T)
    fun onError(exception: Exception?)
    fun onComplete()
}