package com.example.domain.usecase.runner

import com.example.domain.entity.CurrencyEntity
import com.example.domain.repo.PriceRepository
import com.example.domain.usecase.base.BaseUseCase
import kotlinx.coroutines.flow.Flow

class GetCurrencyUseCase constructor(private val postsRepository: PriceRepository) :
    BaseUseCase<Flow<List<CurrencyEntity>>, BaseUseCase.NoParams>() {

    override suspend fun run(params: NoParams): Flow<List<CurrencyEntity>> {
        return postsRepository.getCurrency()
    }

}