package com.example.domain.usecase.base

import com.example.domain.func.handleException
import com.example.domain.usecase.callback.UseCaseCallBack
import kotlinx.coroutines.*
import java.util.concurrent.CancellationException

abstract class BaseUseCase<Type, in Params>() where Type : Any {

    abstract suspend fun run(params: Params): Type


    operator fun invoke(scope: CoroutineScope, params: Params, callback: UseCaseCallBack<Type>) {
        scope.launch {
            try {
                val result = run(params)
                callback.onSuccess(result)
            } catch (exception: CancellationException) {
                exception.printStackTrace()
                callback.onError(handleException(exception))
            } catch (e: Exception) {
                e.printStackTrace()
                callback.onError(handleException(e))
            } finally {
                callback.onComplete()
            }
        }
    }

    /**
     * Don't need to get the response or handle exception
     */
    fun autoRun(scope: CoroutineScope, params: Params) {
        scope.launch {
            try {
                val result = run(params)
                println("result: ${result}")
            } catch (exception: CancellationException) {
                exception.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
            }
        }
    }

    class NoParams
}